import {
	Component,
	OnInit
} from '@angular/core';
import {
	NavController,
	ToastController
} from '@ionic/angular';

class Book {
	title: string;
	author: string;
	price: number;
	resume: string;
	cover: string;
}

@Component({
	selector: 'app-book',
	templateUrl: './book.page.html',
	styleUrls: ['./book.page.scss'],
})
export class BookPage implements OnInit {
	books: Book[];
	rate: number;
	constructor(public toastController: ToastController, private nav: NavController) {}

	goBack() {
		this.nav.back();
	}

	async successToast(event: any) {
		const starInput = event.target.getAttribute("data-star");
		this.rate = starInput;
		let textSupporter = (starInput == 1) ? "estrela" : "estrelas";
		const toast = await this.toastController.create({
			message: `Sua avaliação de ${starInput} ${textSupporter} foi salva!`,
			duration: 2000
		});
		toast.present();
	}

	ngOnInit() {
		this.books = [{
			title: "Harry Potter e a Ordem da Fênix",
			author: "J.K. Rowling",
			resume: "Harry não é mais um garoto. Aos 15 anos, continua sofrendo a rejeição dos Dursdley, sua estranha família no mundo dos 'trouxas'. Também continua contando com Rony Weasley e Hermione Granger, seus melhores amigos em Hogwarts, para levar adiante suas investigações e aventuras. Mas o bruxinho começa a sentir e descobrir coisas novas, como o primeiro amor e a sexualidade. Nos volumes anteriores, J. K. Rowling mostrou como Harry foi transformado em celebridade no mundo da magia por ter derrotado, ainda bebê, Voldemort, o todopoderoso bruxo das trevas que assassinou seus pais. Neste quinto livro da saga, o protagonista, numa crise típica da adolescência, tem ataques de mau humor com a perseguição da imprensa, que o segue por todos os lugares e chega a inventar declarações que nunca deu. Harry vai enfrentar as investidas de Voldemort sem a proteção de Dumbledore, já que o diretor de Hogwarts é afastado da escola. E vai ser sem a ajuda de seu protetor que o jovem herói enfrentará descobertas sobre a personalidade controversa de seu pai, Tiago Potter, e a morte de alguém muito próximo.",
			price: 19.98,
			cover: "book_1.jpg",
		}];

	}

}